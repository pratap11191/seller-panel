import React, { Component } from "react";
import './uikit.css';
import './yodadmincss.css';
import './uikit-rtl.css';
import Sidebar from "./sidebar";
import Header from "./header";
import { Link } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import { ToastContainer, toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import Loader from 'react-loader-spinner';


class Orders extends Component{

    constructor(props) {
        super(props);
       
        if(localStorage.getItem('logindata') === null){
          window.location.assign("./");
       }

         this.state = {data:[]};


         fetch("http://mobuloustech.com/yodapi/api/orderlisting/"+JSON.parse(localStorage.getItem('logindata')).id).then((response) => response.json())
            .then((res) => { 
             //alert(res);
             if(res.status === 'FAILURE'){
                toast.error(res.message);
             } else {
             //toast.success(res.message);
             //alert(res);
             this.setState({data: res.response});
            
             //localStorage.setItem('logindata', res.sellerlogin);
              //this.props.history.push('/');
             }
             console.log(res);
            })
            .catch((error) => {
            console.log(error);
            alert('Oops, something went wrong. Please try again!');
            });
        
      }

      handleDelete = deletedRows => {
        const { data, tableColumns } = this.props;
        const deletedIndexes = Object.keys(deletedRows.lookup);
        //alert([0])
        const data123 = this.state.data;
        console.log(data123);
        deletedIndexes.map(function(name, index){
          fetch("http://mobuloustech.com/yodapi/api/deleteorder/"+data123[name][1]).then((response) => response.json())
            .then((res) => { 
             //alert(res);
             if(res.status === 'FAILURE'){
                toast.error(res.message);
             } else {
             toast.success(res.message);
             //alert(res);
             //this.setState({data: res.response});
            
             //localStorage.setItem('logindata', res.sellerlogin);
              //this.props.history.push('/');
             }
             console.log(res);
            })
            .catch((error) => {
            console.log(error);
            alert('Oops, something went wrong. Please try again!');
            });
          
        })
        
        // const rows = transformToArray(data, tableColumns);
        // deletedIndexes.map(index =>
        //     limitPromisecConcurrency(() => this.remoteDelete(rows[index]))
        // );
    }
      

     
    render(){
        const columns = [
            {name:"DETAILS",
            options: {
                filter: false,
                customBodyRender: (value, status, updateValue) => {
        
                  return (
                    <div class="prodts-tbs">
                                        <div class="drs">
                                            <img src={value[0]}/>
                                        </div>
                                        <div class="drs">
                                            <span class="prne">{value[2]}</span>
                                            <p>Seller SKU: {value[1]}</p>
                                            <p>Product ID: {value[3]}</p>
                                        </div>
                                    </div>
                  );
        
                }
              }
        }, 
            "ORDER ID",
            "BUYER",
            "LOCATION", 
            "ORDER DATE", 
            "DISPATCH BY",
            "PAYMENT TYPE",
            "AMOUNT", 
            {name:"STATUS",
            options: {
                filter: false,
                customBodyRender: (value, status, updateValue) => {
        
                  return (
                   <div>{value[0]}</div>
                  );
        
                }
              }
        }
        ,
        {name:"ACTION",
        options: {
            filter: true,
            customBodyRender: (value, status, updateValue) => {
        
              return (
                <div>
               
                                <Link to={{"pathname":"/ordersdetails/"+value,"id":value}}  class="roundico"><FontAwesomeIcon icon={faEye} /></Link>
                                
               </div>
              );
        
            }
          }
        }];  
      const options = {
        filterType: "dropdown",
        responsive: "scroll",
        search:true,
        onRowsDelete: this.handleDelete,
        textLabels: {
          body: {
              noMatch: this.props.isLoading ?
                    <Loader
                    type="Puff"
                    color="#00BFFF"
                    height="100"
                    width="100"
                /> :
                  'Sorry, no orders yet.',
          },
      },
      };
      
        return <div class="dash-layout">
        <Header />
        
    <div class="bodylayouts-yod">	
        <Sidebar/>
        <div class="pagecontentright">
        <p><ToastContainer /></p>
	<div class="productexp">
		
		
		{/* <div class="prdelements">
			<Link to="/addproduct" class="dashbtns"> Add Product</Link>
		</div> */}
	
	</div>
		
		<div class="yodadm-tablesthm uk-overflow-auto">
        <MUIDataTable
        title={"Order List"}
        data={this.state.data}
        columns={columns}
        options={options}
      />
		</div>
	</div>
        
    </div>
        
    </div>

    }
}


export default Orders;