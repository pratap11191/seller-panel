import React, { Component } from "react";
import './uikit.css';
import './yodadmincss.css';
import './uikit-rtl.css';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCog, faShoppingBasket, faPowerOff } from '@fortawesome/free-solid-svg-icons'

class Sidebar extends Component{

    constructor(props) {
        super(props);
       
        if(localStorage.getItem('logindata') === null){
             window.location.assign("./")
         }
        
         //console.log(JSON.parse(localStorage.getItem('logindata')).id);
      }

      showAlert() {
        localStorage.setItem('logindata', null);
        window.location.assign("./")
      }

    render(){
        return <div class="sidebarleft">
            <ul>
                <li><NavLink to="/dashboard" activeClassName="active"><img alt="hhjj"  src={require('./img/dashico.png')} />Dashboard</NavLink></li>
                <li><NavLink to="/product" activeClassName="active"><img alt="hhjj"  src={require('./img/myprod.png')} />My Products</NavLink></li>
                <li><NavLink to="/orders" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faShoppingBasket}/> Orders</NavLink></li>
                <li><NavLink to="/setting" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faUserCog} /> Account</NavLink></li>
                <li><NavLink onClick={this.showAlert}><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faPowerOff} />Logout</NavLink></li>
               </ul> 
        </div>
    }
}


export default Sidebar;